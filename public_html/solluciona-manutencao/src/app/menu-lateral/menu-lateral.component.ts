import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-lateral',
  templateUrl: './menu-lateral.component.html',
  styleUrls: ['./menu-lateral.component.scss']
})
export class MenuLateralComponent implements OnInit {
  public mostraMenu = false;
  
  constructor() { }

  ngOnInit() {
  }

  toggleMenu() {
    this.mostraMenu = !this.mostraMenu;
  }
}
